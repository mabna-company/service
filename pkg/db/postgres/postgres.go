// Package postgres implements postgres connection.
package postgres

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type Postgres struct {
	DB *gorm.DB
}

// New Will connect to postgres server and return a Postgres object
func New(url string) (*Postgres, error) {

	db, err := gorm.Open(postgres.Open(url), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})

	if err != nil {
		return nil, err
	}

	return &Postgres{DB: db}, nil
}
