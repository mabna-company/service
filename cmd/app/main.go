package main

import (
	"gitlab.com/mabna-company/service/config"
	"gitlab.com/mabna-company/service/internal"
)

func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	internal.RunApplication(cfg)
}
