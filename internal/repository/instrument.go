package repository

import (
	"context"
	entity2 "gitlab.com/mabna-company/service/internal/entity"
	"gitlab.com/mabna-company/service/pkg/db/postgres"
)

type InstrumentRepository struct {
	*postgres.Postgres
}

func New(pg *postgres.Postgres) *InstrumentRepository {
	return &InstrumentRepository{pg}
}

func (i *InstrumentRepository) GetAllInstrumentsLastTrades(ctx context.Context) (results []map[string]interface{}) {

	subQuery := i.DB.Model(&entity2.Trade{}).Select("*").Order("trade.dateen DESC")
	i.DB.Model(&entity2.Instrument{}).Select("distinct on (instrument.name) instrument.name, t.id, t.dateen, t.open, t.high, t.low, t.close").Joins("left join (?) as t on t.instrumentid = instrument.id", subQuery).Scan(&results)

	return results
}
