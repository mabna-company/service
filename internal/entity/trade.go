package entity

// Trade includes some data of trades in a instrument
type Trade struct {
	ID           uint    `json:"id" gorm:"Id"`
	InstrumentId uint    `json:"instrument_id" gorm:"InstrumentId"`
	DateEn       string  `json:"dateen" gorm:"DateEn"`
	Open         float64 `json:"open" gorm:"Open"`
	High         float64 `json:"high" gorm:"High"`
	Low          float64 `json:"low" gorm:"Low"`
	Close        float64 `json:"close" gorm:"Close"`
}
