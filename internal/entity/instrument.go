package entity

// Instrument includes name of financial instruments and have relation to Trade struct
type Instrument struct {
	ID   uint   `json:"id" gorm:"Id"`
	Name string `json:"name" gorm:"name"`
}
