package v1

import "github.com/gin-gonic/gin"

type body struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func response(ctx *gin.Context, statusCode int, data interface{}, message string) {
	ctx.JSON(statusCode, body{
		Status:  statusCode,
		Data:    data,
		Message: message,
	})
}
