package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mabna-company/service/internal/usecase"
)

func NewRouter(handler *gin.Engine, u *usecase.InstrumentUseCase) {

	handler.Use(gin.Recovery())
	h := handler.Group("/v1")
	{
		newInstrumentsRoutes(h, u)
	}
}
