package v1

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/mabna-company/service/internal/usecase"
	"net/http"
)

type instrumentRoutes struct {
	useCase *usecase.InstrumentUseCase
}

type lastTradesResponse struct {
	ID     uint    `json:"Id"`
	Name   string  `json:"name"`
	DateEn string  `json:"dateen"`
	Open   float64 `json:"open"`
	High   float64 `json:"high"`
	Low    float64 `json:"low"`
	Close  float64 `json:"close"`
}

func newInstrumentsRoutes(handler *gin.RouterGroup, u *usecase.InstrumentUseCase) {
	r := &instrumentRoutes{u}
	h := handler.Group("/instruments")
	{
		h.GET("/trades/last", r.getAllInstrumentsLastTrades)
	}
}

func (r *instrumentRoutes) getAllInstrumentsLastTrades(c *gin.Context) {
	trades := r.useCase.GetAllInstrumentsLastTrades(c.Request.Context())
	b, err := json.Marshal(trades)
	if err != nil {
		response(c, http.StatusNotImplemented, nil, err.Error())
		return
	}

	var data []*lastTradesResponse
	if err = json.Unmarshal(b, &data); err != nil {
		response(c, http.StatusNotImplemented, nil, err.Error())
		return
	}

	response(c, http.StatusOK, data, "")
}
