package usecase

import (
	"context"
	"gitlab.com/mabna-company/service/internal/repository"
)

type InstrumentUseCase struct {
	repo *repository.InstrumentRepository
}

func New(r *repository.InstrumentRepository) *InstrumentUseCase {
	return &InstrumentUseCase{
		repo: r,
	}
}

// GetAllInstrumentsLastTrades return last trade of each instrument
func (uc *InstrumentUseCase) GetAllInstrumentsLastTrades(ctx context.Context) []map[string]interface{} {
	return uc.repo.GetAllInstrumentsLastTrades(ctx)
}
