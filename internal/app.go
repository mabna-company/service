package internal

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/mabna-company/service/config"
	v1 "gitlab.com/mabna-company/service/internal/api/http/v1"
	"gitlab.com/mabna-company/service/internal/repository"
	"gitlab.com/mabna-company/service/internal/usecase"
	"gitlab.com/mabna-company/service/pkg/db/postgres"
	"gitlab.com/mabna-company/service/pkg/server/http"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func RunApplication(cfg *config.Config) {

	dbAddr := fmt.Sprintf("host=%s user=%s password=%s dbname=postgres port=%s sslmode=disable", cfg.DB.Host, cfg.DB.User, cfg.DB.Password, cfg.DB.Port)
	pg, err := postgres.New(dbAddr)
	if err != nil {
		return
	}

	instrumentUseCase := usecase.New(
		repository.New(pg),
	)

	handler := gin.New()
	v1.NewRouter(handler, instrumentUseCase)
	httpServer := httpserver.New(cfg, handler)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	select {
	case <-interrupt:
	}

	err = httpServer.Shutdown()
	if err != nil {
		log.Fatalf("error shutdown server : %v", err)
	}

}
